<?php
    function conn(){
        return $db=new PDO('mysql:host=localhost;dbname=exam','charlie','immortal');
   }
   $db=conn();
   $req= $db->query('select * from filiere');
   $filieres=$req->fetchAll();
   $req= $db->query('select * from departements');
   $departements=$req->fetchAll();
   $req= $db->query('select * from communes');
   $communes=$req->fetchAll();
   $req= $db->query('select * from arrondissements');
   $arrondissements=$req->fetchAll();
   $req= $db->query('select * from villages');
   $villages=$req->fetchAll();
   
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <h4>Nouveau CLuster</h4>
        <form class="form" action="./save.php" method="post">
            <label for="filiere"><b>Filiere</b></label><br>
            <select name="filiere" id="filiere">
                <option value="" selected disabled>Selection une filiere</option>
                <?php foreach($filieres as $filiere){ ?>
                 <option value="<?=$filiere['idfiliere'] ?>"><?=$filiere['nom'] ?></option>
                <?php } ?>
            </select><br>
            <div class="flex">
               <div class="mar">
                    <label for="dep"><b>Departement</b></label><br>
                    <select onchange="choix()" name="departement" id="dep">
                        <option value="" selected disabled>Selection un departement</option>
                        <?php foreach($departements as $departement){ ?>
                        <option value="<?=$departement['iddepartement'] ?>"><?=$departement['nom'] ?></option>
                        <?php } ?>
                    </select>
               </div>
                <div class="mar">
                    <label for="com"><b>Commune</b></label><br>
                    <select onchange="choixarrond()" name="commune" id="com">
                        <option value="" selected disabled>Selection une Commune</option>
                        <?php /*foreach($communes as $commune){ ?>
                        <option value="<?=$commune['idcommune'] ?>"><?=$commune['nom'] ?></option>
                        <?php } */?>
                    </select><br>
                </div>
            </div>
           <div class="flex">
               <div class="mar">
                <label for="arrond"><b>Arrondissement</b></label><br>
                    <select onchange="choixvill()" name="arrond" id="arrond">
                        <option value="" selected disabled>Selection un Arrondissement </option>
                        <?php /* foreach($arrondissements as $arrondissement){ ?>
                        <option value="<?=$arrondissement['idarrondissement'] ?>"><?=$arrondissement['nom'] ?></option>
                        <?php } */ ?>
                    </select>
               </div>
               <div >
                <label for="village"><b>Village</b></label><br>
                    <select name="village" id="village">
                        <option value="" selected disabled>Selection un village</option>
                        <?php foreach($villages as $village){ ?>
                        <option value="<?=$village['idvillage'] ?>"><?=$village['nom'] ?></option>
                        <?php } ?>
                    </select><br>
               </div>
           </div>
            <label for="cluster"><b>Cluster</b> </label><br>
            <input type="text" name="cluster" required id="cluster">
            <br>
            <input name="btn" class="btn_enregistrer" type="submit" value="Enregistrer">
            <input type="reset" class="btn_cancel" value="Annuler">
            
        </form>
    </div>
    <script src="./script.js"></script>
    
</body>
</html>